import selenium
import requests
from bs4 import BeautifulSoup
import random
import re
import ast
durl = "https://www.atg.party/sc.txt"
def fetch_values(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    return soup.text

def get_data(id_val):
    value_of_elem = fetch_values("https://www.atg.party/sc.txt")
    lst = re.split('=|\n', value_of_elem)
    it = iter(lst)
    res_dct = dict(zip(it, it))
    k = ast.literal_eval(res_dct[id_val])
    return k

def randomizer(subject):
    j = get_data(subject)
    if (isinstance(j,dict)):
        topic= random.choice(list(j))
        return (topic)
    else:
        location = random.choice(list(j))
        return(location)
