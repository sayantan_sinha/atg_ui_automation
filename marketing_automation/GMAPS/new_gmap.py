from selenium import webdriver
from time import sleep
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
import requests
import keys
class ChromeBrowsing():
    grp=""
    lct=""
    done=False
    def __init__(self):
        prefs = {"profile.default_content_setting_values.notifications": 2}
        option = webdriver.ChromeOptions()
        option.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
        # option.add_argument('headless')
        option.add_experimental_option("prefs", prefs)

        path=r"/home/omprakash/Desktop/GMAP/chromedriver"
        path=r"/Users/saurabhbassi/Desktop/chromedriver2"
        self.browser=webdriver.Chrome(executable_path=path,options=option)
    def goTo(self,URL):
        self.browser.get(URL)
    def setInterest(self,group,location):
        self.setgrp(group)
        self.setloc(location)
    def setgrp(self,group="dance"):
        ##self.grp=keys.randomizer('interests')
        self.grp=group
    def setloc(self,location="bangalore"):
        ##self.lct=keys.randomizer('location')
        self.lct=location
    def getInterest(self):
        return self.grp+" "+self.lct
    def getgrp(self):
        return self.grp
    def getloc(self):
        return self.lct
    def gmaps(self):
        interest=self.getInterest()
        self.goTo("https://www.google.com/maps/search/"+interest+"/")
        while self.done==False:
            self.lenres=self.getLen()
            print(self.lenres)
            self.routine()
        self.browser.quit()
    def routine(self):
        self.browser.implicitly_wait(5)
        res=self.browser.find_elements_by_css_selector("div[data-result-index]")
        
        lenres=len(res)
        
        for i in range(0,lenres):
            lenres=len(res)
            
            I=[]
            self.browser.implicitly_wait(10)
            sleep(1.5)
            res=self.browser.find_elements_by_css_selector("div[data-result-index]")
            
            if(i==len(res)-1):
                
                break
            try:
                res[i].click()
                
            except:
                ActionChains(self.browser).move_to_element(res[i]).click(res[i]).perform()
            
            self.browser.implicitly_wait(10)
            try:
                title=self.getTitle()
            except Exception as e:
                print(e)
                sleep(1)
                res[i].click()
                
                self.browser.implicitly_wait(10)
                sleep(1)
                title=self.getTitle()
            
                
            
            info=self.browser.find_elements_by_css_selector("span[class='section-info-text']>span[class='widget-pane-link']")
            for i in info:
                I.append(i.text)
            try:
                Scraper.scrapresGMAP(I,self.getloc(),self.getgrp(),title)
            except Exception as e:
                print(e)
                pass
            
            self.browser.find_element_by_css_selector("button[jsaction='pane.place.backToList']").click()
        try:
            nextbtn = self.browser.find_element_by_css_selector('button[aria-label=" Next page "]')
        except Exception as e:
            print(e)
            pass
        try:
            if(nextbtn.get_attribute("disabled")=='true'):
                self.done=True
                print("Last Page Results Done")
            else:
                nextbtn.click()
                print("Going To Next Page Now")
                self.browser.implicitly_wait(10)
                
        except Exception as e:
            print(e)
            pass
        
            
    def getTitle(self):
        return self.browser.find_element_by_css_selector("div>h1").text
    def getLen(self):
        return len(self.browser.find_elements_by_css_selector("div[data-result-index]"))
class Scraper():
    
    all_eml=[]
    all_phn=[]
    all_web=[]
    def scrapresGMAP(li,loc,grp,title):
        eorw=[]
        phone=[]
        for i in li:
            try:
                phone.append(int((i).replace(' ', '')))
            except ValueError:
                k = i
                if (
                    k.endswith('.net') or
                    k.endswith('.com') or
                    k.endswith('.in') or
                    k.endswith('.gov') or
                    k.endswith('.org') or
                    k.endswith('.me')):
                    eorw.append(i)
        email =[]
        site =[]
        for l in eorw:
            if '@' in l:
                email.append(l)
            else:
                site.append(l)
                try:
                    site[0].replace('Menu\n','')
                except:
                    pass
        
        print(phone)
        print(email)
        print(site)
        for i in phone:
            if i in Scraper.all_phn:
                phone.remove(i)
            else:
                Scraper.all_phn.append(i)
        for i in email:
            if i in Scraper.all_eml:
                email.remove(i)
            else:
                Scraper.all_eml.append(i)
        for i in site:
            if i in Scraper.all_phn:
                site.remove(i)
            else:
                Scraper.all_web.append(i)
        print("Afterwards")
        print(phone)
        print(email)
        print(site)
        try:
            data1=ApiCaller.updatedata(email,phone)
            if(len(data1)>0):
                if(data1[0]==r"&"):
                    dt="location1="+loc+"&group1="+grp+data1+"&url=gmap "+title
                    print(dt)
                    ApiCaller.new_api_poster(dt)
                else:
                    print("Insufficient data")
            else:
                print("Insufficient data")
        except Exception as e:
            print(e)
        try:
            data2=ApiCaller.updatewebdata(site)
            if(len(data2)>0):
                
                dt="location="+loc+"&group="+grp+data2
                print(dt)
                ApiCaller.web_api_poster(dt)
                
            else:
                print("Insufficient data")
        except Exception as e:
            print(e)
            pass

class ApiCaller():
    def new_api_poster(data):
        rq=requests.post('http://13.71.83.193/api/new-api?'+data)
        print("--"*40)
        if rq.ok:
            print("Data sent and Response text is "+str(rq.content))
        else:
            print("Unsucessful")
        print("--"*40)
    def web_api_poster(data):
        rq=requests.post('http://13.71.83.193/api/website?'+data)
        if rq.ok:
            print("Data sent and Response text is "+str(rq.content))
        else:
            print("Unsucessful")
        print("--"*40)
    def updatedata(email,phone,about=True):
        eml=""
        phn=""
        if len(email)>0:
            eml+=email[0]
        if len(phone)>0:
            phn+=str(phone[0])
        try:
            for i in email:
                if i!=email[0]:
                    eml+=","+i
        except:
            pass

        try:
            for i in phone:
                if i!=phone[0]:
                    phn+=","+i
        except:
            pass
        
        if len(eml)>0:
            eml="&email1="+eml
        if len(phn)>0:
            phn="&phone1="+phn
        if about==True:
            pass
        else:
            pass
        #print(eml)
        #print(phn)
        return eml+phn
    def updatewebdata(website):
        webs=""
        if len(website)>0:
            webs+=website[0]
        try:
            for i in website:
                if i!=website[0]:
                    webs+=","+i
        except:
            pass
        if len(webs)>0:
            webs="&website="+webs
        else:
            pass
        #print(webs)
        return webs
        
C=ChromeBrowsing()
#group="dancing"
#location="mumbai"
group=keys.randomizer('interests')
location=keys.randomizer('location')
C.setInterest(group,location)
interest=C.getInterest()
print(interest)
C.gmaps()

