from random import randrange as rn
from random import uniform as un
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import requests
import re, keys, pdb
from bs4 import BeautifulSoup

class Scraper():
    all_eml=[]
    all_phn=[]
    all_web=[]
    def tag_visible(element):
        if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
            return False
        return True
    def scrapdata(URL):
        link=URL
        page=requests.get(link)
        main_contact=page.content
        main_contact_soup = BeautifulSoup(main_contact, 'html.parser')  #Parse html code
        text_of_main_contact = main_contact_soup.findAll(text=True)
        visible_texts = filter(Scraper.tag_visible, text_of_main_contact)
        return u" ".join(t.strip() for t in visible_texts)
    def getEmails(data):
        mail_list_final=[]
        mail_list = re.findall("\w+@\w+\.{1}\w+", data)
        for i in mail_list:
            if i in Scraper.all_eml:
                pass
            else:
                mail_list_final.append(i)
                Scraper.all_eml.append(i)
        return mail_list_final

    def getPhone(data):
        phone_list_final=[]
        phone_list=re.findall("(?:[+]\s*91|0)?[789](?:\s*\d){9}",data)
        for i in phone_list:
            if i in Scraper.all_phn:
                pass
            else:
                phone_list_final.append(i)
                Scraper.all_phn.append(i)
        return phone_list_final

    def getSites(data):
        site_list_final=[]
        site_list=re.findall(r"(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})",data)
        for i in site_list:
            if("..." in i):
                site_list.remove(i)
            else:
                pass
        for i in site_list:
            if i in Scraper.all_web:
                pass
            else:
                site_list_final.append(i)
                Scraper.all_web.append(i)
        return site_list_final


class Facebook():
    fbpagedir="https://www.facebook.com/directory/pages/"
    grp=""
    lct=""
    pageLst=[]
    def __init__(self):
        option=webdriver.FirefoxOptions()
        profile=webdriver.FirefoxProfile()
        profile.set_preference('permissions.default.image',2)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so','false')
        #option.add_argument("--headless")
        path=r'/home/omprakash/Desktop/geckodriver'
        path=r'/usr/local/bin/geckodriver'
        self.browser=webdriver.Firefox(executable_path=path,firefox_profile=profile,firefox_options=option)
    def goTo(self,URL):
        try:
            self.browser.get(URL)
        except Exception as e:
            print(e)
            pass
    def currURL(self):
        return self.browser.current_url
    def spslp(self,x,y):
        sleep(un(x,y))
    def scroll(self,times):
        for i in range(times):
            self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            self.spslp(2,3)

    def setInterest(self,group,location):
        self.setgrp(group)
        self.setloc(location)
    def setgrp(self,group="dance"):
        ##self.grp=keys.randomizer('interests')
        self.grp=group
    def setloc(self,location="bangalore"):
        ##self.lct=keys.randomizer('location')
        self.lct=location
    def getInterest(self):
        return self.grp+" "+self.lct
    def getgrp(self):
        return self.grp
    def getloc(self):
        return self.lct


    def srch(self,srchterm="Dance Bangalore"):
        self.browser.implicitly_wait(10)
        try:
            e1=self.browser.find_element_by_xpath("//*[@id='q_dashboard']")
            e1.send_keys(srchterm)
            e1.send_keys(Keys.RETURN)
        except Exception as e:
            print(e)
            pass
    def getPageList(self):
        res=self.browser.find_element_by_xpath("//*[@id='pagelet_loader_initial_browse_result']")
        el1=res.find_elements_by_css_selector("a[href]>span")
        for i in el1:
            t1=i.find_element_by_xpath("..")
            h=t1.get_attribute("href")
            if h not in self.pageLst:
                self.pageLst.append(h)
        return self.pageLst
    def scrapFunc(self,about=False):
        body=Scraper.scrapdata(self.currURL())
        em=Scraper.getEmails(body)
        print(em)
        ph=Scraper.getPhone(body)
        print(ph)
        ws=Scraper.getSites(body)
        print(ws)
        if(about==True):
            PAGE_NAME=self.getPageTitle()
            data1="group1="+self.getgrp()+"&location1="+self.getloc()+ApiCaller.updatedata(em,ph)+"&url1=fb_page "+PAGE_NAME
            print(data1)
            print("Sending data as soon as it was found")
            if(len(ph)>0 or len(em)>0):
                ApiCaller.new_api_poster(data1)
            else:
                print("Data not found here, proceeding forward")
        else:
            data1="group1="+self.getgrp()+"&location1="+self.getloc()+ApiCaller.updatedata(em,ph)+"&url1=fb_url"
            print(data1)
            if(len(ph)!=0 or len(em)!=0):
                ApiCaller.new_api_poster(data1)
            else:
                print("Sufficient Data not found here, proceeding forward")
        data2="group="+self.getgrp()+"&location="+self.getloc()+ApiCaller.updatewebdata(ws)
        if(len(ws))>0:
            ApiCaller.web_api_poster(data2)
        else:
            print("Sufficient Data not found here, proceeding forward")
##        print("Emails:")
##        print(em)
##        print("Phone:")
##        print(ph)
##        print("Websites")
##        print(ws)
    def notnowfb(self):
        try:
            if self.browser.find_element_by_css_selector("a[id='expanding_cta_close_button']"):
                self.browser.find_element_by_css_selector("a[id='expanding_cta_close_button']").click() 
        except:
            pass
    def getPageTitle(self):
        try:
            t=self.browser.find_element_by_css_selector("div[id='entity_sidebar'] span div span a span")
        except Exception as e:
            print(e)
        return t.text
    def getAll(self,scroll_times=20):
        try:
            self.getPageList()
        except Exception as e:
            print(e)
            pass
        
        for i in self.pageLst:
            print("Scraping About section\n\n")
            self.goTo(i+"about")
            sleep(1)
            try:
                self.scrapFunc(about=True)
            except Exception as e:
                print(e)
                pass
            print("Scraping Posts section\n\n")
            self.goTo(i+"posts")
            sleep(3)
            try:
                self.scroll(scroll_times)
            except Exception as e:
                print(e)
                pass
            try:
                self.notnowfb()
            except Exception as e:
                print(e)
                pass
            try:
                self.scrapFunc()
            except Exception as e:
                print(e)
                pass
        
            
            
            
            
            
        


class ApiCaller():
    def new_api_poster(data):
        try:
            rq=requests.post('http://13.71.83.193/api/new-api?'+data)
        except Exception as e:
            print(e)
            pass
        print("--"*40)
        if rq.ok:
            print("Data sent and Response text is "+str(rq.content))
        else:
            print("Unsucessful")
        print("--"*40)
    def web_api_poster(data):
        try:
            rq=requests.post('http://13.71.83.193/api/website?'+data)
        except Exception as e:
            print(e)
            pass
        if rq.ok:
            print("Data sent and Response text is "+str(rq.content))
        else:
            print("Unsucessful")
        print("--"*40)
    def updatedata(email,phone,about=True):
        eml=""
        phn=""
        if len(email)>0:
            eml+=email[0]
        if len(phone)>0:
            phn+=phone[0]
        try:
            for i in email:
                if i!=email[0]:
                    eml+=","+i
        except:
            pass

        try:
            for i in phone:
                if i!=phone[0]:
                    phn+=","+i
        except:
            pass
        
        if len(eml)>0:
            eml="&email1="+eml
        if len(phn)>0:
            phn="&phone1="+phn
        if about==True:
            pass
        else:
            pass
        print(eml)
        print(phn)
        return eml+phn
    def updatewebdata(website):
        webs=""
        if len(website)>0:
            webs+=website[0]
        try:
            for i in website:
                if i!=website[0]:
                    webs+=","+i
        except:
            pass
        if len(webs)>0:
            webs="&website="+webs
        else:
            pass
        print(webs)
        return webs

F=Facebook()
group="dancing"
location="ahmedabad"
group=keys.randomizer('interests')
location=keys.randomizer('location')
F.setInterest(group,location)
interest=F.getInterest()
print(interest)
F.goTo(F.fbpagedir)
F.srch(interest)
F.scroll(0)
sleep(2)
F.getAll(20)
F.browser.quit()
