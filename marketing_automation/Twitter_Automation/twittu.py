from random import randrange as rn
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import selenium
import requests
from bs4 import BeautifulSoup
import random
import re
import ast, pickle

durl = "https://www.atg.party/sc.txt"
def fetch_values(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.content, 'html.parser')
    return soup.text

def get_data(id_val):
    value_of_elem = fetch_values("https://www.atg.party/sc.txt")
    lst = re.split('=|\n', value_of_elem)
    it = iter(lst)
    res_dct = dict(zip(it, it))
    k = ast.literal_eval(res_dct[id_val])
    return k

def randomizer(subject):
    j = get_data(subject)
    if (isinstance(j,dict)):
        topic= random.choice(list(j))
        return (topic)
    else:
        location = random.choice(list(j))
        return(location)

URL="https://www.twitter.com"
#URL2="https://twitter.com/search?q=dance%20bangalore&src=typed_query&f=user"
URL2="https://twitter.com/search?q="+randomizer('interests')+"%20"+randomizer('location')+"&src=typed_query&f=user"
"""The above URL2 is to be used with the randomizer function"""

uname="admin@atg.world"#Username here
pwd="Twitter@123"#Enter Password here

class twittu():
    def __init__(self):
        option = webdriver.FirefoxOptions()
        #option.add_argument("--headless")
        #option.add_argument("start-maximized")
        option.add_argument("window-size=1920x1080")
        path='/usr/local/bin/geckodriver'
        self.browser=webdriver.Firefox(executable_path=path,firefox_options=option)
    def visitURL(self,URL):
        self.browser.get(URL)
    def exitBrowser(self):
        self.browser.quit()
    def check_ele_by_css(self,element):
        delay=10
        try:
            ele=WebDriverWait(self.browser,delay).until(EC.presence_of_element_located((By.CSS_SELECTOR,element)))
        except TimeoutError:
            print("sorry not found")
    def ss(self):
        self.browser.save_screenshot("scr.png")
    def login(self,uname,pwd):
        try:
            for cookie in pickle.load(open("TwitterCookies.pkl", "rb")):
                self.browser.add_cookie(cookie)
        except:
            self.check_ele_by_css("input[name='session[username_or_email]']")
            self.check_ele_by_css("input[type='password']")
            uname=self.browser.find_element_by_css_selector("input[name='session[username_or_email]']").send_keys(uname)
            sleep(random.randint(1,2))
            p=self.browser.find_element_by_css_selector("input[name='session[password]']").send_keys(pwd)
            btn=self.browser.find_element_by_css_selector("input[type='submit']").click()
            sleep(random.randint(1,3))
            pickle.dump(self.browser.get_cookies() , open("TwitterCookies.pkl","wb"))
    def scroll(self):
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    def scrltwice(self):
        self.scroll()
        sleep(1)
        #self.scroll()
    def scrollTop(self):
        self.browser.execute_script("window.scrollTo(0, 0);")
    def scale77(self):
        self.browser.execute_script('document.body.style.MozTransform = "scale(0.33)";')
    def store_urls(self):
        self.scale77()
        self.wait()
        accs=self.browser.find_elements_by_css_selector("div[aria-label='Timeline: Search timeline'] div[data-testid='UserCell'] a:not([dir='ltr'])")
        twacc=[]
        for i in accs:
            if i.get_attribute("href") not in twacc:
                twacc.append(i.get_attribute("href"))
        return twacc
    def flwbtn1(self):
        self.wait()
        self.scale77()
        flwbtns=[]
        for i in self.browser.find_elements_by_css_selector("div[aria-label='Timeline: Search timeline'] span span"):
            if i.text=="Follow":
                flwbtns.append(i)
        return flwbtns
    def flwbtn2(self):
        self.scale77()
        self.wait()
        
        flwbtns2=[]
        for i in self.browser.find_elements_by_css_selector("div[aria-label='Timeline: Followers'] span span"):
            if i.text=="Follow":
                flwbtns2.append(i)
        return flwbtns2
    
    def follow(self,flwbtns):
        self.scale77()
        self.scrollTop()
        y=0
            
        for i in flwbtns:
            self.wait()
            self.slp()
            try:
                if random.randint(0,1):
                    ActionChains(self.browser).move_to_element(i).click().perform()
                    print("followed "+str(i))
            except selenium.common.exceptions.MoveTargetOutOfBoundsException:
                y+=500
                self.browser.execute_script('window.scrollTo(0, ' + str(y) + ');')
                sleep(0.5)
                ActionChains(self.browser).move_to_element(i).click().perform()
                
    def followiter(self,urllist):
        for i in range(len(urllist)):
            flw=[]
            self.browser.get(str(urllist[i]+r"/followers"))
            self.wait()
            self.scrltwice()
            self.wait()
            sleep(1)
            self.scrollTop()
            try:
                if random.randint(0,1):
                    flw=self.flwbtn2()
                    print("FOLLOWED ")
            except:
                sleep(3)
                flw=self.flwbtn2()
            self.follow(flw)
    def slp(self):
        sleep(rn(2,3))
    def wait(self):
        self.browser.implicitly_wait(rn(2,5))
    def pgdwn(self):
        pass

t=twittu()
t.visitURL(URL)
t.login(uname,pwd)
t.visitURL(URL2)
urllist=t.store_urls()
flwbtns=t.flwbtn1()
t.follow(flwbtns)
t.followiter(urllist)
t.browser.quit()
