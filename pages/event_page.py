import logging
from utility.services import Services
from selenium.webdriver.common.keys import Keys
from data.data_for_event import DataForEvent


logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.INFO)


class EventPage:
    def __init__(self, driver):
        self.driver = driver
        self.service = Services(self.driver)
        self.url = "https://www.atg.party/event"
        self.title_of_event_txtfield = "id=title"
        self.event_textarea = "xpath=//*[@class='fr-element fr-view']"
        self.contact_name_field = "id=contact_name"
        self.contact_number_field = "id=contact_number"
        self.email_field = "id=email_address"
        self.venue_field = "id=venue"
        self.add_cost_btn = "id=btn_cost"
        self.cost_free_btn = "xpath=//*[@class='title']"
        self.cost_paid_btn = "xpath=(//*[@class='title'])[2]"
        self.single_cost_btn = "xpath=(//*[@class='title'])[3]"
        self.multi_cost_btn = "xpath=(//*[@class='title'])[4]"
        self.online_btn = "xpath=(//*[@class='title'])[5]"
        self.next_btn_in_cost = "id=nextbtn"
        self.price_field_single_ticket = "id=cost"
        self.category_field1_multi = "xpath=//*[@name='advanced_opt_category[0]']"
        self.description_field1_multi= "xpath=//*[@name='advanced_opt_description[0]']"
        self.price_field1_multi_ticket = "id=advanced_opt_cost"
        self.category_field2_multi = "xpath=//*[@name='advanced_opt_category[2]']"
        self.description_field2_multi= "xpath=//*[@name='advanced_opt_description[2]']"
        self.price_field2_multi_ticket = "id=advanced_opt_cost2"
        self.new_ticket_btn_multi = "id=btAdd"
        self.cost_done_btn = "id=nextbtn"
        self.start_date_field = "id=start_dt"
        self.start_time = "id=start_tm"
        self.tags_field = "id=tag-tokenfield"
        self.group_field = "xpath=//*[@class='popover-tag-wrapper']"
        self.onclick_group_python = "xpath=//li[@data-value='181']"
        self.post_btn = "xpath=//button[text()='Post']"

    def click_post(self):
        post_btn=self.service.find_element(self.post_btn)
        self.driver.execute_script("arguments[0].click();", post_btn)

    def filldata(self):
        self.driver.get(self.url)
        data = DataForEvent()
        title = self.service.find_element(self.title_of_event_txtfield)
        title.send_keys(data.title)
        text = self.service.find_element(self.event_textarea)
        text.send_keys(data.text)
        contact_name = self.service.find_element(self.contact_name_field)
        contact_name.send_keys(data.name)
        contact_number = self.service.find_element(self.contact_number_field)
        contact_number.send_keys(data.number)
        email = self.service.find_element(self.email_field)
        email.send_keys(data.email)
        venue = self.service.find_element(self.venue_field)
        venue.send_keys(data.venue)
        self.service.click_element(self.start_date_field)
        date = self.service.find_element(self.start_date_field)
        date.send_keys(Keys.RETURN)
        self.service.click_element(self.start_time)
        start_time = self.service.find_element(self.start_time)
        start_time.send_keys(Keys.RETURN)
        tags = self.service.find_element(self.tags_field)
        tags.send_keys(data.tags)
        self.service.click_element(self.group_field)
        self.service.click_element(self.onclick_group_python)

    def post_as_free(self):
        self.filldata()
        add_cost_btn=self.service.find_element(self.add_cost_btn)
        self.driver.execute_script("arguments[0].click();", add_cost_btn)
        
        cost_free_btn=self.service.find_element(self.cost_free_btn)
        self.driver.execute_script("arguments[0].click();", cost_free_btn)

        cost_done_btn=self.service.find_element(self.cost_done_btn)
        self.driver.execute_script("arguments[0].click();", cost_done_btn)

        self.click_post()

    def post_as_paid_single_ticket(self):
        self.filldata()

        add_cost_btn=self.service.find_element(self.add_cost_btn)
        self.driver.execute_script("arguments[0].click();", add_cost_btn)
        
        cost_paid_btn=self.service.find_element(self.cost_paid_btn)
        self.driver.execute_script("arguments[0].click();", cost_paid_btn)

        single_cost_btn=self.service.find_element(self.single_cost_btn)
        self.driver.execute_script("arguments[0].click();", single_cost_btn)

        self.driver.execute_script("document.getElementById('cost').value='200'")
        
        online_btn=self.service.find_element(self.online_btn)
        self.driver.execute_script("arguments[0].click();", online_btn)
        
        cost_done_btn=self.service.find_element(self.cost_done_btn)
        self.driver.execute_script("arguments[0].click();", cost_done_btn)
        
        self.click_post()

    def post_as_paid_multi_ticket(self):
        self.filldata()

        add_cost_btn=self.service.find_element(self.add_cost_btn)
        self.driver.execute_script("arguments[0].click();", add_cost_btn)
        
        cost_paid_btn=self.service.find_element(self.cost_paid_btn)
        self.driver.execute_script("arguments[0].click();", cost_paid_btn)

        self.service.wait_for_element(self.multi_cost_btn)

        multi_cost_btn=self.service.find_element(self.multi_cost_btn)
        self.driver.execute_script("arguments[0].click();", multi_cost_btn)

        data = DataForEvent()
        self.driver.execute_script("document.getElementById('advanced_opt_cost').value='400'")

        cat1 = self.service.find_element(self.category_field1_multi)
        self.driver.execute_script("arguments[0].value='hackathon';", cat1)

        des1 = self.service.find_element(self.description_field1_multi)
        self.driver.execute_script("arguments[0].value='a hackathon';", des1)

        new_ticket_btn_multi=self.service.find_element(self.new_ticket_btn_multi)
        self.driver.execute_script("arguments[0].click();", new_ticket_btn_multi)

        price2 = self.service.find_element(self.price_field2_multi_ticket)
        self.driver.execute_script("arguments[0].value='600';", price2)

        cat2 = self.service.find_element(self.category_field2_multi)
        self.driver.execute_script("arguments[0].value='codathon';", cat2)

        des2 = self.service.find_element(self.description_field2_multi)
        self.driver.execute_script("arguments[0].value='a codathon';", des2)

        opt_online=self.service.find_element(self.online_btn)
        self.driver.execute_script("arguments[0].click();", opt_online)

        cost_done_btn=self.service.find_element(self.cost_done_btn)
        self.driver.execute_script("arguments[0].click();", cost_done_btn)

        self.click_post()